
# TP1 - Mise en jambes

  

Pour ce TP, je tiens à préciser que je suis sur Fenêtres 10 et que le Firewall a été désactivé.

  

## I. Exploration locale en solo

# I. Exploration locale en solo

  

## [](#1-affichage-dinformations-sur-la-pile-tcpip-locale)1. Affichage d'informations sur la pile TCP/IP locale

  

### [](#en-ligne-de-commande)En ligne de commande

  

En utilisant la ligne de commande (CLI) de votre OS :

  

**🌞 Affichez les infos des cartes réseau de votre PC**

  

ipconfig /all

- Intel(R) Wireless-AC 9560 160MHz,14-4F-8A-BF-4E-FB,10.33.18.106

- Realtek PCIe GBE Family Controller,98-28-A6-16-1F-C4, pas d'IP car la carte réseau n'est pas utilisée

  

**🌞 Affichez votre gateway**

  

`ipconfig | findstr "Passerelle par défaut"`

  

- 10.33.19.254

  

### [](#en-graphique-gui-graphical-user-interface)En graphique (GUI : Graphical User Interface)

  

En utilisant l'interface graphique de votre OS :

  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

  

`Centre Réseau et partage`

`Etat de WI-FI`

`Détails`

  

- IP:10.33.18.106,

- MAC :14-4F-8A-BF-4E-FB,

- Passerelle :10.33.19.254

  

### [](#questions)Questions

  

- 🌞 à quoi sert la [gateway](/it4lik/b2-reseau-2022/main/../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

Elle sert a connecter le réseau Ynov au réseau Internet

  

## [](#2-modifications-des-informations)2. Modifications des informations

  

### [](#a-modification-dadresse-ip-part-1)A. Modification d'adresse IP (part 1)

  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

  

- changez l'adresse IP de votre carte WiFi pour une autre

- ne changez que le dernier octet

- par exemple pour `10.33.1.10`, ne changez que le `10`

- valeur entre 1 et 254 compris

### 1. Affichage d'informations sur la pile TCP/IP locale

- Nom, adresse MAC et adresse IP de l'interface WiFi :

```

PS C:\Users\lucas> ipconfig /all

[...]

Carte réseau sans fil Wi-Fi :

[...]

Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560

Adresse physique . . . . . . . . . . . : 16-4F-8A-BF-4E-FB

[...]

Adresse IPv4. . . . . . . . . . . . . .: 10.188.245.200(préféré)

```

- Nom, adresse MAC et adresse IP de l'interface Ethernet :

```

PS C:\Users\lucas> ipconfig /all

[...]

Carte Ethernet Ethernet :

Statut du média. . . . . . . . . . . . : Média déconnecté

Description. . . . . . . . . . . . . . : Killer Realtek PCIe GBE Family Controller

Adresse physique . . . . . . . . . . . : 98-28-A6-16-1F-C4

```

Les adresses physiques correspondent aux adresses MAC, les descriptions aux noms et l'adresse IPv4 à l'adresse IP.

Le port Ethernet n'étant pas utilisé, il n'y a pas d'adresse IPv4 à fournir

  

- Affichage de la passerelle :

```

PS C:\Users\gympl> ipconfig /all

[...]

Carte réseau sans fil Wi-Fi :

[...]

Passerelle par défaut. . . . . . . . . : 10.188.0.1

```

- Pour trouver l'IP, la MAC et la gateway pour l'interface WiFi de votre PC en graphique, accéder à ce chemin :

1. Panneau de configuration\Réseau et Internet\Connexions réseau

2. Cliquer sur la carte Wifi puis sur Détails...

3. Lire

```

Description: Intel(R) Wireless-AC 9560

Adresse physique: 14-4F-8A-BF-4E-FB

Adresse IPv4: 10.188.245.200

Passerelle par défaut IPv4: 10.188.0.1

```

- La gateway d'Ynov sert à sortir du réseau local d'Ynov pour aller sur Internet ou un autre réseau (quand on est connecté dessus bien sûr).

  

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

1. Panneau de configuration\Réseau et Internet\Connexions réseau

2. Clique droit sur la carte wifi puis propriétés

3. Cliquer sur Protocole Internet version 4 (TCP/IPv4) puis propriétés

4. Dans Général modifier l'IP manuellement

```

Adresse IP : 10.188.245.64

Longueur du préfixe de sous-réseau : 255.255.252.0

Passerelle : 10.188.0.1

```

Si l'adresse IP choisie est déjà utilisé par un autre utilisateur du réseau alors je ne pourrai pas me connecter car je serai dans l'incapacité de recevoir une réponse.

## II. Exploration locale en duo

  

### 1. Modification d'adresse IP

  

Via la même manipulation mais cette fois sur Ethernet j'ai pu changer l'adresse IP :

  

```

PS C:\Users\gympl> ipconfig

[...]

Carte Ethernet Ethernet :

[...]

Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.102

Masque de sous-réseau. . . . . . . . . : 255.255.255.252

```

Test ping d'une machine à l'autre :

```

PS C:\Users\Lucas> ping 192.168.1.102

  

Envoi d’une requête 'Ping' 192.168.1.102 avec 32 octets de données :

Réponse de 192.168.1.102 : octets=32 temps=1 ms TTL=128

```

Affichage de la table ARP :

```

PS C:\Users\lucas> arp -a

[...]

Interface : 192.168.1.101 --- 0x6

Adresse Internet Adresse physique Type

192.168.1.102 98-28-a6-16-1f-c4 dynamique

192.168.1.103 ff-ff-ff-ff-ff-ff statique

224.0.0.22 01-00-5e-00-00-16 statique

224.0.0.251 01-00-5e-00-00-fb statique

224.0.0.252 01-00-5e-00-00-fc statique

239.255.255.250 01-00-5e-7f-ff-fa statique

255.255.255.255 ff-ff-ff-ff-ff-ff statique

[...]

```

  

### 2. Utilisation d'un des deux comme gateway

  

Sur le PC qui où Internet est désactivé :

```

Modification de la passerelle par l'ip du pc ayant la connection à savoir :

192.168.1.102

```

Sur le PC qui où Internet est activé :

```

Dans le panneau de configuration -> Réseau et Internet -> Etat de Wifi -> Propriétés de Wifi-> Partage -> activer le partage de wifi via Ethernet.

```

Résolution de ping vers serveur google :

```

PS C:\Users\Lucas> ping 8.8.8.8 Envoi d’une requête 'Ping' 8.8.8.8 avec 32 octets de données :

Réponse de 8.8.8.8 : octets=32 temps=25 ms TTL=113

```

Utilisation traceroute

```

PS C:\Users\Lucas> tracert google.com

Détermination de l’itinéraire vers google.com [216.58.215.46] avec un maximum de 30 sauts :

1 <1 ms * <1 ms LAPTOP-2P72Q5OG [192.168.137.1]

```

### 3. Petit chat privé

PC Serveur :

```

PS C:\Users\Lucas\Desktop\netcat-1.11> .\nc.exe -l -p 8888

```

PC Client :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> .\nc.exe 192.168.137.2 8888

```

  

Discussion intéréssante :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> .\nc.exe 192.168.137.2 8888

fpobs

gros fdp

téo c un boffon

```

Allons plus loin dans le chat :

```

PS C:\Users\Lucas\Desktop\netcat-1.11> .\nc.exe -l -p 8888 192.168.137.1

```

Seul l'IP marquée dans la commande peut se connecter

  

### 4. Firewall

  

Autorisation des pings :

Dans un powershell en mode admin :

```

netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow

```

PC 1 :

![Screen PC 1](https://i.ibb.co/tp26hyr/image-2022-09-28-161716071.png)

PC 2

![Screen PC 2](https://cdn.discordapp.com/attachments/887698327066529842/1024685734868815892/2022-09-28_16_13_57-Pare-feu_Windows_Defender.png)

Autorisation Netcat sur le port 8888:

```

PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name= "Open Port 8888" dir=in action=allow protocol=TCP localport=8888

Ok.

```

## III. Manipulations d'autres outils/protocoles côté client

  

### 1. DHCP

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> ipconfig /all

[...]

Carte réseau sans fil Wi-Fi :

[...]

Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254

```

Bail expirant du DHCP :

```

Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 15:01:50

```

  
  

### 2. DNS

  

Mon DNS :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> ipconfig /all

[...]

Carte réseau sans fil Wi-Fi :

[...]

Serveurs DNS. . . . . . . . . . . . . : 8.8.8.8

8.8.4.4

1.1.1.1

```

  

#### nslookup :

- Google :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> nslookup google.com

Serveur : dns.google

Address: 8.8.8.8

  

Réponse ne faisant pas autorité :

Nom : google.com

Addresses: 2a00:1450:4007:806::200e

216.58.213.78

```

A l'adresse IP 216.58.213.78 se trouve le nom de domaine google.com avec comme adresse MAC 2a00:1450:4007:806::200e

  

- Ynov :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> nslookup ynov.com

Serveur : dns.google

Address: 8.8.8.8

  

Réponse ne faisant pas autorité :

Nom : ynov.com

Addresses: 2606:4700:20::681a:be9

2606:4700:20::681a:ae9

2606:4700:20::ac43:4ae2

104.26.10.233

172.67.74.226

104.26.11.233

```

Aux adresses IP 104.26.10.233, 172.67.74.226, 104.26.11.233 se trouve le nom de domaine ynov.com avec comme adresses MAC 2606:4700:20::681a:be9

2606:4700:20::681a:ae9

2606:4700:20::ac43:4ae2

  

#### Reverse lookup

  

- 78.74.21.21

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> nslookup 78.74.21.21

Serveur : dns.google

Address: 8.8.8.8

  

Nom : host-78-74-21-21.homerun.telia.com

Address: 78.74.21.21

```

A l'adresse IP 78.74.21.21 se trouve le nom de domaine homerun.telia.com

  

- 92.146.54.88

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> nslookup 92.146.54.88

Serveur : dns.google

Address: 8.8.8.8

  

*** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain

```

L'adresse IP 92.146.54.88 ne correspondt à aucun nom de domaine.

  

## IV. Wireshark

  

- Ping entre moi et la passerelle :

```

233 64.960857 192.168.137.2 192.168.137.1 ICMP 74 Echo (ping) request id=0x0001, seq=227/58112, ttl=128 (reply in 234)

```

  

- Message Netcat entre moi et l'autre PC :

```

42 12.742014 192.168.137.2 192.168.137.1 TCP 64 8888 → 33377 [PSH, ACK] Seq=1 Ack=5 Win=2097920 Len=10

```

  

- Requête DNS :

```

70 50.668970314 192.168.137.2 8.8.4.4 DNS 100 Standard query 0x288f A clientservices.googleapis.com OPT

```

c'est une requête aux serveurs DNS de google.
