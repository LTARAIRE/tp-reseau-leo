


# TP2 : Ethernet, IP, et ARP

## I. Setup IP
### 🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

Adresse IP choisies :
PC 1 :
```
192.168.1.65/26
```
PC 2 :
```
192.168.1.66/26
```
Masque de sous réseau : 255.255.255.192
Adresse Réseau : 192.168.1.64/26
Adresse Broadcast : 192.168.1.127/26
### 🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**
ping :
```
PS C:\Windows\system32> ping 192.168.1.66

Envoi d’une requête 'Ping'  192.168.1.66 avec 32 octets de données :
Réponse de 192.168.1.66 : octets=32 temps=1 ms TTL=128
```
### 🌞 **Wireshark it**

Le type de Paquet ICMP est un "echo"

## II. ARP my bro
### 🌞 **Check the ARP table**
Afficher la table ARP :
```
PS C:\Windows\system32> arp -a
```
Adresse MAC du binôme :
La MAC correspond  à l'adresse physique ci-dessous :
```
[...]
Interface : 192.168.1.65 --- 0x6
  Adresse Internet      Adresse physique      Type
  192.168.1.66          98-28-a6-16-1f-c4     dynamique
[...]
```
Adresse MAC de la gateway :
La MAC correspond  à l'adresse physique ci-dessous (celle d'Ynov) :
```
[...]
Interface : 10.33.18.82 --- 0xa
  Adresse Internet      Adresse physique      Type
  [...]
10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  [...]
```
### 🌞 **Manipuler la table ARP**
Vider la table ARP :
```
PS C:\Windows\system32> arp -d
```
- Avant :
```
PS C:\Windows\system32> arp -a

Interface : 10.5.1.90 --- 0x3
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.1.65 --- 0x6
  Adresse Internet      Adresse physique      Type
  192.168.1.66          98-28-a6-16-1f-c4     dynamique
  192.168.1.127         ff-ff-ff-ff-ff-ff     statique
  192.168.137.155       98-28-a6-16-1f-c4     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.33.18.82 --- 0xa
  Adresse Internet      Adresse physique      Type
  10.33.16.59           80-30-49-b6-da-5d     dynamique
  10.33.16.252          68-ec-c5-49-7f-4b     dynamique
  10.33.18.90           d8-fc-93-30-f1-7e     dynamique
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
- Après :
```
PS C:\Windows\system32> arp -a

Interface : 10.5.1.90 --- 0x3
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.1.65 --- 0x6
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.33.18.82 --- 0xa
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
```
- Après un ping : 
```
Interface : 192.168.1.65 --- 0x6
  Adresse Internet      Adresse physique      Type
  192.168.1.66          98-28-a6-16-1f-c4     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
```
On voit que l'adresse Internet 192.168.1.66 réapparaît

###  🌞 **Wireshark it**
- Première Trame :
Adresse : 192.168.1.65 (adresse IP du pc envoyant le ping)

Destination : Broadcast (tout le monde)

- Deuxième Trame :
Adresse : Adresse mac du pc recevant le ping (CompalIn_16:1f:c4)

Destination : Adresse mac du pc envoyant le ping (CompalIn_e8:e7:c7)

## III. DHCP you too my brooo
###  🌞 **Wireshark it**
- Première Trame (Discover):
	Source : 0.0.0.0

	Destination : 255.255.255.255
- Deuxième Trame (Offer):
	Source : 10.33.19.254
	
	Destination : 10.33.19.106
	
- Troisième Trame (Request):
	Source : 0.0.0.0

	Destination : 255.255.255.255
- Adresse Gateway :
10.33.19.254

- DNS proposés :
	8.8.8.8
	8.8.4.4
	1.1.1.1
- Adresse IP proposé :
	10.33.18.106
	
- Quatrième Trame (ACK):
	Source : 10.33.19.254

	Destination : 10.33.18.106
	
## IV. Avant-goût TCP et UDP
###  🌞 **Wireshark it**

L'IP utilisé par youtube est : 20.54.24.231
Le port utilisé par Youtube est le port 443
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTkwMzE4NTEyN119
-->