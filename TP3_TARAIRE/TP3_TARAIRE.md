﻿### 1. Echange ARP

🌞**Générer des requêtes ARP**

```
[wazdorixs@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.522 ms
```
```
[wazdorixs@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.521 ms
```

Table ARP de John:
```
[wazdorixs@localhost ~]$ ip neigh show
10.3.1.12 dev enp0s3 lladdr 08:00:27:50:49:55 REACHABLE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:32 REACHABLE
```
Mac de Marcel :
```
08:00:27:50:49:55
```

Table ARP de Marcel:
```
[wazdorixs@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:32 REACHABLE
10.3.1.11 dev enp0s3 lladdr 08:00:27:d5:27:82 REACHABLE
```
Mac de John:
```
08:00:27:d5:27:82
```

MAC de Marcel de chez John
```
[wazdorixs@localhost ~]$ ip neigh show 10.3.1.12
10.3.1.12 dev enp0s3 lladdr 08:00:27:50:49:55 STALE
```

Mac de Marcel chez Marcel
```
[wazdorixs@localhost ~]$ ip a | grep ether
    link/ether 08:00:27:50:49:55 brd ff:ff:ff:ff:ff:ff
```

### 2. Analyse de trames

🌞**Analyse de trames**

vider table arp:
```
[wazdorixs@localhost ~]$ sudo ip neigh flush all
```
tcpdump

```
[wazdorixs@localhost ~]$ sudo tcpdump -w tp2_arp.pcapng
```

ping
```
[wazdorixs@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.764 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.498 ms
```

II. Routage

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**

`[wazdorixs@localhost ~]$cat /proc/sys/net/ipv4/ip_forward`
`1`

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

`[wazdorixs@localhost ~]$ sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s3`
`[wazdorixs@localhost ~]$ sudo ip route add 10.3.2.0/24 via 10.3.2.254 dev enp0s8`

John vers Marcel 

```
[wazdorixs@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.03 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=0.913 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=0.815 ms
^C
--- 10.3.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 0.815/0.919/1.031/0.088 ms
[wazdorixs@localhost ~]$
```

### 2. Analyse de trames

🌞**Analyse des échanges ARP**

| ordre | type trame  | IP source | MAC source                  | IP destination | MAC destination             |
|-------|-------------|-----------|-----------------------------|----------------|-----------------------------|
| 1     | Requête ARP | x         |`john`  `08:00:27:d5:27:82` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         |`routeur`  `0a:00:27:00:00:18`| x              | `john`  `08:00:27:d5:27:82` |
| 1     | Requête ARP | x         |`routeur`  `0a:00:27:00:00:18`| x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         |`Marcel`  `08:00:27:50:49:55` | x              |`routeur`  `0a:00:27:00:00:18`|
| ?     | Ping        |`10.3.1.11`|`john`  `08:00:27:d5:27:82` |`10.3.2.12` |`Marcel`  `08:00:27:50:49:55` |
| ?     | Pong        |`10.3.2.12`|`Marcel`  `08:00:27:50:49:55` |`10.3.1.11` |`john`  `08:00:27:d5:27:82` |

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**
    -   vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
    -   puis avec un `ping` vers un nom de domaine
```
[wazdorixs@localhost ~]$ ip a | grep enp0s9
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.0.4.15/24 brd 10.0.4.255 scope global dynamic noprefixroute enp0s9
[wazdorixs@localhost ~]$
```

Chez Marcel
```
[wazdorixs@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.12
NETMASK=255.255.255.0

GATEWAY=10.3.2.254
DNS=1.1.1.1
```

Chez John :
```
[wazdorixs@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.11
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
DNS=1.1.1.1
```
```
[wazdorixs@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=57 time=12.3 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=57 time=11.4 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=57 time=11.4 ms
^C
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 11.431/11.713/12.264/0.389 ms
```

```
[wazdorixs@localhost ~]$ dig youtube.com

; <<>> DiG 9.16.23-RH <<>> youtube.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 22131
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;youtube.com.                   IN      A

;; ANSWER SECTION:
youtube.com.            100     IN      A       216.58.213.78

;; Query time: 12 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Sat Jan 07 11:48:53 CET 2023
;; MSG SIZE  rcvd: 56
```
```
[wazdorixs@localhost ~]$ ping youtube.com
PING youtube.com (216.58.213.78) 56(84) bytes of data.
64 bytes from lhr25s01-in-f14.1e100.net (216.58.213.78): icmp_seq=1 ttl=118 time=12.0 ms
64 bytes from lhr25s01-in-f14.1e100.net (216.58.213.78): icmp_seq=2 ttl=118 time=12.0 ms
64 bytes from lhr25s01-in-f14.1e100.net (216.58.213.78): icmp_seq=3 ttl=118 time=12.1 ms
^C
--- youtube.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 11.977/12.045/12.143/0.071 ms
```




