from scapy.all import *
import sys
import time

Domain=input('Enter domain: ')

def DNSSniffer(pkt):
    try:
        if DNSQR in pkt and pkt.dport==53:
            print('DNS Querie: ')
            if pkt[DNS].qd.qname:
                print(str(pkt[DNS].qd.qname))
                if Domain in str(pkt[DNS].qd.qname):
                    print('Domain find')
        elif DNSRR in pkt and pkt.sport==53:
            print('DNS Response: ')
            if pkt[DNS].qd.qname:
                print(str(pkt[DNS].qd.qname))
    except KeyboardInterrupt():
        pass
sniff(iface='enp0s3',filter='udp and port 53', store=0, prn=DNSSniffer)