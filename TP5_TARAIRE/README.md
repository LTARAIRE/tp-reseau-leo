# TP5 : MITM (Man In The Middle)

Ce TP a pour but de créér un programme permettant d'intercepter le traffic d'un réseau et de pourvoir modifier les paquets interceptés.

## I.Installation

Dans un premier temps , intallez les outils nécessaire à l'éxecution du programme.

Dans un terminal tapez :
```
apt install python3-pip
pip3 install scapy
```

*Python3* étant le language qu'utilise le programme et *Scapy* qui est la librairie permettant de manipuler les packets réseau.

Ensuite, vous devez cloner le répertoir ci-dessous sur votre machine : 
```
https://gitlab.com/LTARAIRE/mitm.git
```

## II.Utilisation

Pour lancer l'attaque il suffit de lancer le programme *mitm.py*

```
sudo python3 mitm.py
```
Il faut ensuite rentrer l'adresse IP de la machine "Victime" et de l'IP de la passerelle.

### DNS Sniff

Une fois l'attaque effectuée , en lançant *DNS.py*.

Pour cela tapez dans un autre terminal : 
```
sudo python3 DNS.py
```
Vous devez entrez le *Domaine* que vous souhaitez repérer et ensuite vous verrez toutes les requêtes DNS entre votre "Victime" et Internet.